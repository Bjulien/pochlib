
var newDiv = document.createElement("div");
newDiv.setAttribute ("id", "newDiv");
let elts = document.getElementsByClassName('h2');
elts[0].after(newDiv);

newButton ("addBookButton", "Ajouter un livre", newDiv);   // bouton "Ajouter un livre"  
addBookButton.setAttribute('class', 'addbookb'); 

var contentDiv = document.createElement("div");       
let theContent = document.getElementById ("content");
theContent.appendChild(contentDiv);
contentDiv.setAttribute ("id", "contentDiv");


ReloadPage();                                        // Vérifier si la page est rafraichit 

DeleteBook();                                        // Si des livres sont retirés de la Poch' liste


function newButton (buttonId, buttonText, buttonBlock){ /*création de nouveau bouton*/

  var buttonResearch = document.createElement("button") ;
  buttonResearch.setAttribute ("id", buttonId);

  var tx = document.createTextNode(buttonText);  
  buttonResearch.appendChild(tx);  
  buttonBlock.appendChild(buttonResearch);
}

function newForm (formSearch, name, id, labelTxt){    /*Création de formulaire*/
  let myForm= document.createElement("p");
  formSearch.appendChild(myForm);
  var newField = document.createElement("input") ;
  newField.setAttribute("type","search");
  newField.setAttribute("name",name);
  newField.setAttribute("id",id);
  newField.style = "margin:1%";
  var newLabel = document.createElement("label");
  newLabel.style ="text-align:left; margin-left:1%";
  var lt = document.createTextNode(labelTxt); 
  newLabel.appendChild(lt);
  newLabel.setAttribute("for",id);
  myForm.appendChild(newLabel);
  myForm.appendChild(newField);

}



function newResult (resultValue, researchDiv){  /*Affichage des livres*/

  let affichage = '<hr>';
  affichage += '<h2>Resultats de recherche </h2>';
  if (resultValue.totalItems =='0'){ 

    researchDiv.innerHTML = '<h3>Aucun livre n’a été trouvé</h3>';
  }
  else {

    affichage += '<div id= "books">';
    for (let number of resultValue.items){
      
      affichage+= `<section class="bookR" id=${number.id}>`;
      affichage+= '<button class="clickB"><i class="fas fa-bookmark"></i> Ajouter Livre</button>';
      affichage+= `<h3 id="bookId">id: ${number.id}</h3>`; 
      affichage+= `<h3>Titre: ${number.volumeInfo.title}</h3>`;
      affichage+= `<h3>Auteur: ${number.volumeInfo.authors[0]}</h3>`;

      if (number.volumeInfo.description === undefined){

        affichage+= '<p>Description: Information manquante</p>';
      }
      else if (number.volumeInfo.description.length<= 200){

        affichage+= `<p>Descrition: ${number.volumeInfo.description}</p>`;
      }
      else {
        affichage+= `<p>Descrition: ${number.volumeInfo.description.substr(0,200)}...</p>`;
      }

      if (number.volumeInfo.imageLinks === undefined) {
        affichage+='<p style="text-align:center"><img src ="unavailable.png" class= "imageB"/></p>';
      }

      else {
        affichage+= `<p style="text-align:center"><img src= ${number.volumeInfo.imageLinks.smallThumbnail} alt = "image" class= "imageB"/></p>`;
      }

  
      affichage+= '</section>';
    }
  affichage += '</div>';
  researchDiv.innerHTML = affichage;

  }
}


function bookSelect (contentDiv, buttonBook, bookSection, value){ /*Ajout ou suppression d'un livre de la Poch'liste*/
    
   
  for (let i=0; i< buttonBook.length; i++ ){
  
    buttonBook[i].addEventListener('click',function(){                      //Ajout de Livre
      sectionId=bookSection[i].getAttribute ("id");
      let myId = document.getElementById (`sectionthis${sectionId}`);

      if (contentDiv.contains(myId)){
        setTimeout(function() {
          alert('Vous ne pouvez pas ajouter deux fois le même livre')
        });

      }

      else {
        var mySection = bookSection[i].cloneNode(true);

        mySection.setAttribute("id", `sectionthis${sectionId}`);
        contentDiv.appendChild(mySection);
        
        var trashBook = document.createElement ("button");
        trashBook.setAttribute("class", "trashIcon");
        
        trashBook.innerHTML= '<i class="fas fa-trash-alt"></i> Retirer le Livre';
        
        trashBook.setAttribute("id", `this${sectionId}`);

        mySection.replaceChild(trashBook,buttonBook[value.items.length]);
        sessionStorage.setItem(`sectionthis${sectionId}`, mySection.outerHTML);

        console.log(`sectionthis${sectionId}`);
        console.log (`this${sectionId}`);

        trashBook.addEventListener ('click',function(){                      //Suppression de livre

          contentDiv.removeChild(mySection);
          thisSection= mySection.getAttribute ("id");
          console.log (thisSection);
          sessionStorage.removeItem(thisSection); 
        });
      }
    });
  }
}




function ReloadPage(){ 

  if (performance.navigation.type == performance.navigation.TYPE_RELOAD) { //en cas de rafraichissement de la page
   let storageAffich ="";
    
    for (const [key, value] of Object.entries(sessionStorage)) { // récupération de la poch'liste
      
      console.log (storageAffich);
      console.log({
        key, value
      });
      let storage = sessionStorage.getItem(key);
      storageAffich += storage;   
    }
    contentDiv.innerHTML=storageAffich; 
  }
}

function DeleteBook() { //suppression d'un ou plusieurs livres
  for (const [key, value] of Object.entries(sessionStorage)) { 
    
    let sectionId = document.getElementById (key);
    console.log (sectionId);
    let trashBookId =key.slice(7);
    console.log (trashBookId);
    var trashBookStorage  = document.getElementById (trashBookId);
    trashBookStorage.addEventListener ('click', function(){
      contentDiv.removeChild(sectionId);
      sessionStorage.removeItem(key);
    });
  }}

addBookButton.addEventListener('click', function() {     // événement pour le click du bouton "Ajouter un livre"
  
  addBookButton.style.display= "none";
  
  let formSearch = document.createElement("form");
  newDiv.appendChild(formSearch);
  
  
  newForm (formSearch, "title","titleField","Titre du livre:"); //Création des imputs du formulaire
  newForm(formSearch,"author", "authorField","Auteur:");
  
  
  let research= document.createElement ("p");
  research.setAttribute ("id", "researchButtons");
  newDiv.appendChild(research);

  let cancel= document.createElement ("p");
  research.setAttribute ("id", "cancelButtons");
  newDiv.appendChild(cancel);
  
  
  newButton ("buttonResearch", "Rechercher",research); //boutons "Rechercher"
  buttonResearch.setAttribute('class', 'researchb');
  


  newButton ("buttonCancel", "Annuler",cancel); //boutons "Annuler"
  buttonCancel.setAttribute('class', 'cancelb');

  
  var newResearch = document.createElement("div");
  newDiv.after(newResearch);

  buttonResearch.addEventListener('click', function() { //écoute pour le bouton "Rechercher"

    let title = titleField.value;
    let author = authorField.value;
    if (title!= '' && author!='' ){

       fetch(`https://www.googleapis.com/books/v1/volumes?q=intitle:${title},inauthor:${author}&Key=AIzaSyBSCkoZa48-eZeJfUz72GlTkhUIFCS6lxg`)    // requete API Google Books
     
      .then(function(res) {
        if (res.ok) {
          return res.json();
        // console.log(res.json);
        }
      })
      .then(function(value) {
        console.log(value);
       
        newResult (value, newResearch);            // affichage du resultat
    
    
        let buttonBook = document.getElementsByClassName('clickB');
        let bookSection = document.getElementsByClassName('bookR');
        bookSelect (contentDiv, buttonBook, bookSection, value);  //Ajout ou suppression d'un livre à la Poch'liste
      })
    
      .catch(function(err) {
    
        console.log(err);
        // Une erreur est survenue
      });
     
    }
    else  { 
      newResearch.innerHTML = '<h4 style="color:red">Champs vides</h4>';
    }

  });
  
  buttonCancel.addEventListener('click', function() {  //écoute pour le bouton "Annuler"
  
    newDiv.removeChild(formSearch); 
    newDiv.removeChild(research); 
    newDiv.removeChild(cancel); 
  
    let elm = document.getElementById("myBooks");
    addBookButton.style.display= "inline-block";
    elm.removeChild(newResearch); 
  });
  
});




  
