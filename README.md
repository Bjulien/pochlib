<h3>Poch'lib est une application de recherche de livre sur l'API Google Books demandée par "La plume enchantée".</h3>

<h2>Installation de l'application:</h2>

<p>Pour installer, extraire tous les fichiers dans un même dossier ou espace de travail.</p>
<p>Pour lancer l'application, ouvrez le fichier "Pochlib.html" avec un navigateur web.</p>

<h2>Modifier le projet:</h2>

<ul>
  <li>Les fichiers peuvent être modifié avec un éditeur de texte comme Notepad.</li> 
  <li>Ouvrez les fichiers "Pochlib.html" "poch.css" et "index.js" avec l'éditeur de texte.</li>
  <li>Pour changer le logo de l'application, vous pouvez remplacer "logo.png" par n'importe quelle image avec le même nom et format.</li>
</ul>

 <h2>Utilisation : </h2>
<ul>
                             <li>Utilisez le boutton <strong>Ajouter un livre</strong> pour ouvrir le formulaire de recherche (entrez un titre de livre et un nom d'auteur).</li>
                             <li>Utilisez le boutton <strong>Rechercher</strong> pour appliquer la requête sur l'API Google Books et afficher une liste de resultats, le boutton <strong>Annuler</strong> retire le formulaire.</li>
                             <li>Sur la liste de resultats, le boutton <strong>Ajouter Livre</strong> affiche le livre et ses informations dans la section <strong>Ma poch'liste</strong> (il n'est pas possible d'ajouter plus d'une fois le même livre).</li>
                             <li>Dans la section <strong>Ma poch'liste</strong>, le boutton <strong>Retirer le livre</strong> efface le livre et ses information de la section.</li> 
</ul>
